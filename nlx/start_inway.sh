echo "Starting on tcp port: 50060"
docker run --rm \
--name inway_apinovumcjib \
--volume /home/nov_cjib/platform/system/public_html/api.cjib.demo.novum.nu/nlx/root.crt:/certs/root.crt:ro \
--volume /home/nov_cjib/platform/system/public_html/api.cjib.demo.novum.nu/nlx/org.crt:/certs/org.crt:ro \
--volume /home/nov_cjib/platform/system/public_html/api.cjib.demo.novum.nu/nlx/org.key:/certs/org.key:ro \
--volume /home/nov_cjib/platform/system/public_html/api.cjib.demo.novum.nu/nlx/service-config.toml:/service-config.toml:ro \
--env DIRECTORY_REGISTRATION_ADDRESS=directory-registration-api.demo.nlx.io:443 \
--env SELF_ADDRESS=api.cjib.demo.novum.nu:50050 \
--env SERVICE_CONFIG=/service-config.toml \
--env TLS_NLX_ROOT_CERT=/certs/root.crt \
--env TLS_ORG_CERT=/certs/org.crt \
--env TLS_ORG_KEY=/certs/org.key \
--env DISABLE_LOGDB=1 \
--publish 50050:8443 \
nlxio/inway:latest
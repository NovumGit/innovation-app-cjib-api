<?php

return [
    'api_version' => 2,
    'config_dir'  => 'novum.cjib',
    'namespace'   => 'ApiNovumCjib',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => isset($_SERVER['IS_DEVEL']) ? 'api.cjib.demo.novum.nuidev.nl' : 'api.cjib.demo.novum.nu',
    'test_domain' => 'api.test.cjib.demo.novum.nu',
    'dev_domain'  => 'api.cjib.innovatieapp.nl'

];

